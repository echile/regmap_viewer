import qm35720_B0_regmap
import pandas as pd

def get_blocks(rm):
    blocks = []
    for k, v in rm.items():
        if isinstance(v, dict):
            if 'type' in v and v['type'] == 'block':
                blocks.append(k)

    return blocks

def get_register_info(rm, reg, val, blocks):
    reg_info = {}
    for block in blocks:
        if reg in rm[block]:
            print(rm[block][reg])
            fields = get_fields(rm[block][reg])
            reg_info = parse_fields(rm[block][reg], fields, val)
            break
    return reg_info

def parse_fields(reg_map, fields, val):
    parsed = {'fields': [], 'offset':[], 'width':[], 'value':[], 'doc':[]}
    for field in fields:
        if ':' in reg_map[field]['offset']:
            offset = reg_map[field]['offset'].split(':')
            end = int(offset[0])
            start = int(offset[1])
        else:
            end = int(reg_map[field]['offset'])
            start = int(reg_map[field]['offset'])

        bits = end - start + 1
        f = (val >> start) & mask_bits(bits)
        print(f'{field}:{f:x}')
        parsed['fields'].append(field)
        parsed['offset'].append(start)
        parsed['width'].append(bits)
        parsed['value'].append(hex(f))
        parsed['doc'].append(reg_map[field].get('doc', ''))
         

    return parsed

def mask_bits(bits):
    return 2**(bits) - 1




def get_fields(reg):
    fields = []
    for k, v in reg.items():
        if isinstance(v, dict):
            if 'type' in v and v['type'] == 'field':
                fields.append(k)

    return fields

def main():
    rm = qm35720_B0_regmap.top_chip['dut']

    blocks = get_blocks(rm)

    with open('ch9_a.txt', 'r') as f:
        lines = f.readlines()

    
    contents = ''
    for line in lines:
        if '[' in line and ']' in line:
            addr = int(line.split('[')[1].split(']')[0].strip(), 16)
            name = line.split('[')[0].strip()
            val = int(line.split('=')[1].strip(), 16)
            print(f'{name}:{addr:08x}:{val:08x}')
            reg_info = get_register_info(rm, name, val, blocks)

            df = pd.DataFrame(reg_info)
            contents += df.to_html(index=False) + '\n<br>\n'

    with open('test.html', 'w', encoding="utf-8") as f:
        f.write(contents)

if __name__ == '__main__':
    main()